#!/usr/bin/python3.6
import json
import argparse
import sys


def factor_points(x):
    """Função que returna o número de fatores"""
    factor = 2
    factor_list = []
    while x != 1:
        if x % factor == 0:
            x = x/factor
            factor_list.append(factor)
        else:
            factor = factor + 1
    return len(factor_list)


def main(x):
    """
    Função que plota o número de fatores
    """
    if not 10E3 < x < 10E9:
        raise Exception("Value must be between 10^3 and 10^9")
    print(factor_points(x))
    return

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="A List Game", description="Retorna número fatorado"
    )
    parser.add_argument("x", type=int)
    args = parser.parse_args()
    try:
        main(args.x)
    except Exception as e:
        print(e)
        sys.exit(1)
