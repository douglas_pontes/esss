#!/usr/bin/python3.6

import unittest
from main import factor_points, main


class PrimesTestCase(unittest.TestCase):
    """Tests for `main.py`."""

    def test_retorna_ex_1(self):
        """Retorna 16 fatores"""
        self.assertEqual(factor_points(65536), 16)

    def test_retorna_ex_2(self):
        """Retorna 3 fatores"""
        self.assertEqual(factor_points(127381), 3)

    def test_retorna_ex_3(self):
        """Retorna 23 fatores"""
        self.assertEqual(factor_points(725594112), 23)

    def test_retorna_ex_4(self):
        """Retorna Exception"""
        with self.assertRaises(Exception) as error:
            main(15)
        self.assertEqual(
            str(error.exception), "Value must be between 10^3 and 10^9"
        )

if __name__ == '__main__':
    unittest.main()
