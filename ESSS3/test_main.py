#!/usr/bin/python3.6

import unittest
from main import converte_arquivo_para_lista, main


class PrimesTestCase(unittest.TestCase):
    """Tests for `main.py`."""

    def test_retorna_ex_1(self):
        """Retorna lista"""
        resposta = [
            [4, 6, 5, 4, 2, 9, 4, 7, 5, 3, 2, 1, 3, 4, 4, 4, 3, 7, 9, 7],
            [3, 2, 5, 2, 2, 3, 2, 3, 5, 3]
        ]
        self.assertEqual(converte_arquivo_para_lista("sample1.txt"), resposta)

    def test_retorna_ex_2(self):
        """Retorna lista"""
        resposta_1 = [
            [4, 6, 5, 4, 2, 9, 4, 7, 5, 3, 2, 1, 3, 4, 4, 4, 3, 7, 9, 7]
        ]
        self.assertEqual(
            converte_arquivo_para_lista("sample4.txt"), resposta_1
        )

    def test_retorna_ex_3(self):
        """Retorna lista"""
        resposta_2 = [[3, 2, 5, 2, 2, 3, 2, 3, 5, 3]]
        self.assertEqual(
            converte_arquivo_para_lista("sample5.txt"), resposta_2
        )

    def test_retorna_ex_4(self):
        """Retorna Exception"""
        with self.assertRaises(Exception) as error:
            main("sample2.txt")
        self.assertEqual(
            str(error.exception), "Invalid character found: 'a'"
        )

    def test_retorna_ex_5(self):
        """Retorna Exception"""
        with self.assertRaises(Exception) as error:
            main("sample3.txt")
        self.assertEqual(
            str(error.exception), "Multiple '*' found"
        )

    def test_retorna_ex_6(self):
        """Retorna Exception"""
        with self.assertRaises(Exception) as error:
            main("sample6.txt")
        self.assertEqual(
            str(error.exception), "Inconsistent line size"
        )

    def test_retorna_ex_7(self):
        """Retorna Exception"""
        with self.assertRaises(Exception) as error:
            main("sample7.txt")
        self.assertEqual(
            str(error.exception), "Inconsistent line size"
        )

if __name__ == '__main__':
    unittest.main()
