#!/usr/bin/python3.6
import json
import argparse
import sys
import os


def converte_arquivo_para_lista(arquivo):
    """Converte o arquivo em lista"""
    row = 1
    logs, records = ([], [])
    line_size = None
    with open(arquivo, "r") as file_object:
        while True:
            line = file_object.readline()
            if not line:
                break
            if not records:
                records = [None] * (len(line)-1)
            # Acha tamanho de linha inconscistente
            if line_size is None:
                line_size = len(line)
            elif line_size != len(line) and len(line) != 1:
                raise Exception("Inconsistent line size")
            else:
                line_size = len(line)
            # Acha equivalência entre linha e coluna
            for col, char in enumerate(line):
                if char not in [os.linesep, ".", "*"]:
                    raise Exception(f"Invalid character found: '{char}'")
                if char == "*":
                    if records[col] is not None:
                        raise Exception("Multiple '*' found")
                    records[col] = row
            row += 1
            # Novo log
            if line == os.linesep:
                logs.append(records)
                records = []
                line_size = None
                row = 1
        logs.append(records)
    return logs


def plota_log(logs):
    """Plota os logs"""
    for i, log in enumerate(logs):
        for row in range(1, max(log)+1):
            for col in log:
                if col == row:
                    print("*", end="")
                else:
                    print(".", end="")
            print('')
        if i < len(logs)-1:
            print()


def main(arquivo):
    """Arquivo principal"""
    logs = converte_arquivo_para_lista(arquivo)
    for log in logs:
        log.sort(reverse=True)
    plota_log(logs)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="Charting Progress", description="Retorna logs"
    )
    parser.add_argument("x", type=str)
    args = parser.parse_args()
    try:
        main(args.x)
    except Exception as e:
        print(e)
        sys.exit(1)
