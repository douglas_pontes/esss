#!/usr/bin/python3.6
import json
import argparse
import sys


def raiz(x):
    """Encontra a raiz de um número"""
    interval = [1, 100]
    epsilon = 10E-5
    while epsilon < interval[1]-interval[0]:
        M = (interval[1] + interval[0])/2
        if M**2 > x:
            interval[1] = M
        elif M**2 < x and interval[1]**2 > x:
            interval[0] = M
        else:
            interval[1] = interval[1] + 1
    M = (interval[1] + interval[0])/2
    return round(M, 3)


def main():
    """Função que chama a raiz quadrada de um número"""
    if len(sys.argv) == 1:
        raise Exception("Missing number argument")
    x = sys.argv[1]
    if not perfect_int(x):
        raise Exception("Input must be a number")
    else:
        print(raiz(int(x)))


def perfect_int(x):
    """Verifica se a string é contém apenas números"""
    try:
        value = int(x)
        return True
    except Exception:
        return False

if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        print(e)
        sys.exit(1)
