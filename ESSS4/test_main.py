#!/usr/bin/python3.6

import unittest
from main import raiz, main


class PrimesTestCase(unittest.TestCase):
    """Tests for `main.py`."""

    def test_retorna_ex_1(self):
        """Retorna raiz 3.162"""
        self.assertEqual(raiz(10), 3.162)

    def test_retorna_ex_2(self):
        """Retorna raiz 4.472"""
        self.assertEqual(raiz(20), 4.472)

    def test_retorna_ex_3(self):
        """Retorna raiz 3.000"""
        self.assertEqual(raiz(9), 3.000)

    def test_retorna_ex_4(self):
        """Retorna 23 fatores"""
        self.assertEqual(raiz(22500), 150)
    """
    def test_retorna_ex_5(self):
        """'Retorna Exception'"""
        with self.assertRaises(Exception) as error:
            main()
        self.assertEqual(
            str(error.exception), "Missing number argument"
        )
    """

if __name__ == '__main__':
    unittest.main()
