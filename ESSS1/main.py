#!/usr/bin/python3.6
import json
import argparse
import sys
import numbers


def fizz_buzz(number):
    """Função que retorna Buzz, Fizz BuzzFizz ou o próprio número"""
    resto_tres = number % 3
    resto_cinco = number % 5
    if resto_tres == 0 and resto_cinco != 0:
        return "Fizz"
    elif resto_tres != 0 and resto_cinco == 0:
        return "Buzz"
    elif resto_tres == 0 and resto_cinco == 0:
        return "FizzBuzz"
    else:
        return number


def main(x, y):
    """
    Função que encontra os multiplos de cada número de uma lista dada:
        -Se o número for multiplo de três o programa plota Fizz.
        -Se o número for multiplo de cinco o programa plota Buzz.
        -Se o número for multiplo de três e cinco o programa plota FizzBuzz.
        -Se o número não for multiplo de nenhum dos dois números o programa
        plota o próprio número.
    """
    if x < 1 or y < 1:
        raise Exception("X é menor que 1 ou Y é menor que 1")
    if y > 2000:
        raise Exception("Invalid input, Y > 2000")
    for number in range(x, y+1):
        print(fizz_buzz(number))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="FizzBuzz", description="Retorna Fizz Buzz"
    )
    parser.add_argument("x", type=int)
    parser.add_argument("y", type=int)
    args = parser.parse_args()
    try:
        main(args.x, args.y)
    except Exception as e:
        print(e)
        sys.exit(1)
