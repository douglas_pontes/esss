#!/usr/bin/python3.6

import unittest
from main import fizz_buzz, main


class PrimesTestCase(unittest.TestCase):
    """Tests for `main.py`."""

    def test_retorna_ex_1(self):
        """Retorna Fizz"""
        self.assertEqual(fizz_buzz(3), "Fizz")

    def test_retorna_ex_2(self):
        """Retorna Buzz"""
        self.assertEqual(fizz_buzz(5), "Buzz")

    def test_retorna_ex_3(self):
        """Retorna FizzBuzz"""
        self.assertEqual(fizz_buzz(15), "FizzBuzz")

    def test_retorna_ex_4(self):
        """Retorna FizzBuzz"""
        self.assertEqual(fizz_buzz(1), 1)

    def test_retorna_ex_5(self):
        """Retorna Exception"""
        with self.assertRaises(Exception) as error:
            main(-1, 30)
        self.assertEqual(
            str(error.exception), "X é menor que 1 ou Y é menor que 1"
        )

    def test_retorna_ex_6(self):
        """Retorna Exception"""
        with self.assertRaises(Exception) as error:
            main(1, 3000)
        self.assertEqual(
            str(error.exception), "Invalid input, Y > 2000"
        )

if __name__ == '__main__':
    unittest.main()
